module Scraping
  class Response

    class << self

      def formatted_response name, dimension
        { name: name, dimension: dimension, status: :ok }
      end

      def handle_exception message, status
        { error: message, status: status }
      end

      def raise_not_found_exception
        raise ActiveRecord::RecordNotFound.new('No matching record found')
      end
      
    end

  end
end
