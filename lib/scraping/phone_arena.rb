module Scraping
  class PhoneArena < Scraping::Base

    PHONEARENA_BASE_URL = 'https://www.phonearena.com'

    class << self

      def scrap_site query
        doc = open_url "/search?query=#{query}"
        phones = doc.css('#phones') 
        first_result = phones[0].children.try(:css, '.s_fst').try(:first) || Scraping::Response.raise_not_found_exception
        scrap_dimension first_result.try(:css, '.s_thumb')[0]['href']

        rescue ActiveRecord::RecordNotFound => e
          Scraping::Response.handle_exception e.message, :not_found
        rescue Exception => e
          Scraping::Response.handle_exception e.message, :bad_request
      end

      def scrap_dimension path
        doc = open_url path
        name =  doc.css("#phone h1 span").first.try(:text)
        phone_specificatons = doc.try(:css, '#phone_specificatons .s_specs_box')
        dimension = phone_specificatons.first.try(:css, 'ul').try(:xpath, "//li/ul/li/span[//*[contains(@title, 'Dimensions')]]").try(:first).try(:next).try(:text)
        Scraping::Response.raise_not_found_exception if name.blank? && dimension.blank?
        Scraping::Response.formatted_response name, dimension
        
        rescue ActiveRecord::RecordNotFound => e
          Scraping::Response.handle_exception e.message, :not_found
        rescue Exception => e
          Scraping::Response.handle_exception e.message, :bad_request
      end

      private

      def open_url path
        Nokogiri::HTML(open("#{PHONEARENA_BASE_URL}#{path}")) 
      end
      
    end
    
  end
end
