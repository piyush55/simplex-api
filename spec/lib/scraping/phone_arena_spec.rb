require "rails_helper"

describe Scraping::PhoneArena do

  context "Scraping::PhoneArena.scrap_site" do
  
    it 'should return name and dimesion' do
      expect(Scraping::PhoneArena).to receive(:open_url).with('/search?query=zte').and_return(Nokogiri::HTML(open(Rails.root.join('spec/fixtures/phonearena/search.html'))))
      expect(Scraping::PhoneArena).to receive(:open_url).with('/phones/ZTE-Zpad_id10428').and_return(Nokogiri::HTML(open(Rails.root.join('spec/fixtures/phonearena/detail.html'))))
      result = Scraping::PhoneArena.scrap_site 'zte'
      expect(result[:name]).to eq "ZTE Zpad"
      expect(result[:dimension]).to eq "10.20 x 6.69 x 0.31 inches  (259 x 170 x 8 mm)"
      expect(result[:status]).to eq :ok
    end

    it 'should return no matching record found' do
      expect(Scraping::PhoneArena).to receive(:open_url).with('/search?query=abkcls').and_return(Nokogiri::HTML(open(Rails.root.join('spec/fixtures/phonearena/not_found.html'))))
      result = Scraping::PhoneArena.scrap_site 'abkcls'
      expect(result[:error]).to eq "No matching record found"
      expect(result[:status]).to eq :not_found
    end

    it "should return bad request" do
      expect(Scraping::PhoneArena).to receive(:open_url).with('/search?query=').and_return(Nokogiri::HTML(open(Rails.root.join('spec/fixtures/phonearena/bad_request.html'))))
      result = Scraping::PhoneArena.scrap_site ''
      expect(result[:status]).to eq :bad_request
    end
    
  end
  
end
