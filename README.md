# README

## Environment

* Ruby 2.4.0
* Rails 5.0.3

## Configuration

### Clone the repo at local machine:
  * git clone https://piyush55@bitbucket.org/piyush55/simplex-api.git

### Setup

#### Install bundler and run `bundle install`:

```bash
gem install bundler && bundle
```

### How to run the application

```bash
 rails s
