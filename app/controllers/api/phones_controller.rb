class Api::PhonesController < ApplicationController
  
  def index
    render json: { error: 'Invalid query' }, status: :bad_request and return if params[:query].blank?
    phone_detail = Scraping::PhoneArena.scrap_site params[:query]
    render json: phone_detail, status: phone_detail.delete(:status)
    rescue  => e
      render json: { error: e.message }, status: :bad_request
  end

end
